import rclpy
from rclpy.node import Node
from std_msgs.msg import Float32MultiArray
from tf2_ros import TransformBroadcaster
from geometry_msgs.msg import TransformStamped
from scipy.spatial.transform import Rotation as R
from copy import deepcopy as cp
import numpy as np


class GloveVisNode(Node):

    def __init__(self):
        super().__init__('glove_vis_node')
        self.get_logger().info('Starting Glove visualization node')

        # tf related
        self.tf_broadcaster = TransformBroadcaster(self)
    
        self.glove_joint_subscriber = self.create_subscription(Float32MultiArray, '/manus/joints', self.glove_joints_callback, 10)
        self.glove_skel_subscriber = self.create_subscription(Float32MultiArray, '/manus/skeleton', self.glove_skel_callback, 10)
    
    def glove_joints_callback(self, msg:Float32MultiArray):
        pass

    def glove_skel_callback(self, msg:Float32MultiArray): 
        links = {}

        base = "world"
        link = "thumb0"
        for i in range(4):
            trans = msg.data[7*i:7*i + 3]
            quat = msg.data[7*i + 3:7*(i+1)]

            if i == 0:
                trans = [0.0, 0.0, 0.0]
            
            links[link] = {"trans":trans, "quat":quat}

            # self.broadcast_frame(base, link, trans, quat)
            base = cp(link)
            link = "thumb" + str(i+1)


        # Compute CMC stuff
        thumb1_m = R.from_quat(links["thumb0"]["quat"]).as_matrix()
        rotated = np.matmul(thumb1_m, [0.0, 0.0, 0.1])

        # self.broadcast_frame("world", "cmc", rotated, [1.0, 0.0, 0.0, 0.0])

        hand_rotate = R.from_euler("x", 0, degrees=True)
        hand_rotate_m = hand_rotate.as_matrix()

        cmc1_rot = R.from_euler("y", 19.5, degrees=True)
        cmc1_rot_m = cmc1_rot.as_matrix()

        # thumb_coord = R.from_matrix(np.matmul(hand_rotate_m, cmc1_rot_m)).as_quat()

        # self.broadcast_frame("world", "thumb frame", [0.0, 0.0, 0.0], thumb_coord)

        # cmc1_aligned: measured point in the frame aligned to CMC1
        hand_aligned = np.matmul(rotated, hand_rotate_m)
        cmc1_aligned = np.matmul(hand_aligned, cmc1_rot_m)

        # project cmc1_aligned to x-y plane
        # cmc1_projected = np.array([0, cmc1_aligned[1], cmc1_aligned[2]])
        cmc1_projected = np.array([cmc1_aligned[0], cmc1_aligned[1], 0])

        # Get angle of CMC1: x as the 'x' axis and -y as the 'y' axis
        cmc1 = np.degrees(np.arctan2(-cmc1_projected[1], cmc1_projected[0]))

        cmc2_rot = R.from_euler("z", -cmc1, degrees=True)
        cmc2_rot_m = cmc2_rot.as_matrix()

        cmc2_aligned = np.matmul(cmc1_aligned, cmc2_rot_m)

        # Get angle of CMC2: x as the 'x' axis and z as the 'y' axis
        cmc2 = np.degrees(np.arctan2(cmc2_aligned[2], cmc2_aligned[0]))

        print("cmc1:", round(cmc1, 2), " cmc2:", round(cmc2, 2))


    def broadcast_frame(self, base_name, frame_name, trans, quat):

        t = TransformStamped()
        t.header.stamp = self.get_clock().now().to_msg()
        t.header.frame_id = base_name
        t.child_frame_id = frame_name

        t.transform.translation.x = trans[0]
        t.transform.translation.y = trans[1]
        t.transform.translation.z = trans[2]

        t.transform.rotation.x = quat[0]
        t.transform.rotation.y = quat[1]
        t.transform.rotation.z = quat[2]
        t.transform.rotation.w = quat[3]

        self.tf_broadcaster.sendTransform(t)


def main(args=None):
    rclpy.init(args=args)

    glove_controller_node = GloveVisNode()

    rclpy.spin(glove_controller_node)

    glove_controller_node.destroy_node()
    rclpy.shutdown()


if __name__ == '__main__':
    main()