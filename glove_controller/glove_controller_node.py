import rclpy
from rclpy.node import Node
from std_msgs.msg import Float32MultiArray
from sensor_msgs.msg import JointState
from scipy.spatial.transform import Rotation as R
import numpy as np

class GloveTeleopNode(Node):

    def __init__(self):
        super().__init__('glove_teleop_node')
        self.get_logger().info('Starting Glove teleoperation node')

        # Subscriber
        self.glove_joint_subscriber = self.create_subscription(Float32MultiArray, '/manus/joints', self.glove_joints_callback, 10)
        self.glove_skel_subscriber = self.create_subscription(Float32MultiArray, '/manus/skeleton', self.glove_skel_callback, 10)
        
        # Publisher
        self.hand_joint_demand_publisher = self.create_publisher(JointState, '/adapt/hand/joint_demand', rclpy.qos.qos_profile_sensor_data)

        # Loopback
        self.mainloop = self.create_timer(0.05, self.mainloop_callback) 

        # Common values
        self.cmc = None
        self.joint_data = None

    def mainloop_callback(self):
        if (self.joint_data is None) or (self.cmc is None):
            return
        
        all_joint_pos = {}

        finger_order = ["Thumb", "Index", "Middle", "Ring", "Pinky"]
        finger_joint_order = ["MCP_Spread", "MCP", "PIP", "DIP"]
        thumb_joint_order = ["CMC1", "CMC2", "MCP", "IP"]

        for j, finger_name in enumerate(finger_order):

            if finger_name == "Thumb":
                base_index = 20 + j*4

                thumb_MCP = self.joint_data[base_index + 2]
                thumb_IP = self.joint_data[base_index + 3]
                
                angles = [self.cmc[0], self.cmc[1], thumb_MCP, thumb_IP]

                for name, angle in zip(thumb_joint_order, angles):
                    all_joint_pos[finger_name+"_"+name] = angle

            else:
                for k, joint in enumerate(finger_joint_order):
                    index = 20 + j*4 + k
                    joint_name = finger_name + "_" + joint

                    # if joint == "MCP":
                    #     all_joint_pos[joint_name] = self.joint_data[index] * 0.8
                    # else:
                    #     all_joint_pos[joint_name] = self.joint_data[index]

                    all_joint_pos[joint_name] = self.joint_data[index]

        # for key, value in all_joint_pos.items():
        #     print(key, value, type(value))

        # print("-------")
        
        output = JointState()
        output.header.stamp = self.get_clock().now().to_msg()
        output.position = list(all_joint_pos.values())
        output.name = list(all_joint_pos.keys())

        self.hand_joint_demand_publisher.publish(output)


    def glove_joints_callback(self, msg:Float32MultiArray):
        self.joint_data = msg.data

    def glove_skel_callback(self, msg:Float32MultiArray): 
        # offset raw data to match thumb location

        
        # Compute CMC stuff
        thumb1_m = R.from_quat(msg.data[3:7]).as_matrix()
        cmc_raw = np.matmul(thumb1_m, [0.0, 0.0, 0.1])

        cmc_raw[0] -= 0.02
        cmc_raw[1] -= 0.01
        cmc_raw[2] -= 0.02     

        cmc1_rot_m = R.from_euler("y", 10.5, degrees=True).as_matrix()

        # cmc1_aligned: measured point in the frame aligned to CMC1
        cmc1_aligned = np.matmul(cmc_raw, cmc1_rot_m)

        # project cmc1_aligned to x-y plane
        cmc1_projected = np.array([cmc1_aligned[0], cmc1_aligned[1], 0])

        # Get angle of CMC1: x as the 'x' axis and -y as the 'y' axis
        cmc1 = np.degrees(np.arctan2(-cmc1_projected[1], cmc1_projected[0]))

        cmc2_rot_m = R.from_euler("z", -cmc1, degrees=True).as_matrix()

        cmc2_aligned = np.matmul(cmc1_aligned, cmc2_rot_m)

        # Get angle of CMC2: x as the 'x' axis and z as the 'y' axis
        cmc2 = np.degrees(np.arctan2(cmc2_aligned[2], cmc2_aligned[0]))

        # print("cmc1:", round(cmc1, 2), " cmc2:", round(cmc2, 2))
        self.cmc = [cmc1, cmc2]

def main(args=None):
    rclpy.init(args=args)

    glove_teleop_node = GloveTeleopNode()

    rclpy.spin(glove_teleop_node)

    glove_teleop_node.destroy_node()
    rclpy.shutdown()


if __name__ == '__main__':
    main()