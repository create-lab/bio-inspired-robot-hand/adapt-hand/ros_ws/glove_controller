from setuptools import setup

package_name = 'glove_controller'

setup(
    name=package_name,
    version='0.0.0',
    packages=[package_name],
    data_files=[
        ('share/ament_index/resource_index/packages',
            ['resource/' + package_name]),
        ('share/' + package_name, ['package.xml']),
    ],
    install_requires=['setuptools'],
    zip_safe=True,
    maintainer='kaijunge',
    maintainer_email='kai.junge@epfl.ch',
    description='TODO: Package description',
    license='TODO: License declaration',
    tests_require=['pytest'],
    entry_points={
        'console_scripts': [
            'glove_controller_node = glove_controller.glove_controller_node:main',
            'glove_vis_node = glove_controller.glove_vis_node:main'
        ],
    },
)
